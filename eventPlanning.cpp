#include<cstdio>

int main()
{
	int N, B, H, W;
	while(scanf("%d%d%d%d", &N, &B, &H, &W) == 4)
	{
		int hotel[H][W+1];
		for(int j = 0; j < H ; j++)
		{
			scanf("%d", &hotel[j][0]); 
			for(int i = 1; i <= W; i++)
			{
				scanf("%d", &hotel[j][i]);
			}
		}
		
		int order[H];
		int copy[H];
		
		for(int i = 0; i < H ; i++ )
		{
			order[i] = i;
		}
		
		for(int i = 0; i < H ; i++ )
		{
			copy[i] = hotel[i][0];
		}
		
		for(int j = 1; j < H; j++)
		{
			int key = copy[j];
			int orderK = order[j];
			int i = j - 1;
			while(copy[i] > key && i >= 0)
			{
				order[i+1] = order[i];
				copy[i+1] = copy[i];
				i--;
			}
			i++;
			order[i] = orderK;
			copy[i] = key;
		}
		
		int cost  = 0;
		bool b = false;
		
		for(int i = 0; i < H; i++)
		{
			for(int j = 1; j <= W ; j++)
			{
				if(hotel[order[i]][j] >= N && B >= N*hotel[order[i]][0])
				{
					cost = N*hotel[order[i]][0];
					b = true;
					break;
				}
			}
			if(b) break;
		}

		if(cost == 0 ) printf("stay home\n");
		else printf("%d\n", cost);
	
	}
}